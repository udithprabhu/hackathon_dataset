
from __future__ import print_function
import keras
import numpy as np
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras import backend as K
import cv2
from glob import glob
import pickle
import h5py
import datetime

def shuffle(a, b, seed):
   rand_state = np.random.RandomState(seed)
   rand_state.shuffle(a)
   rand_state.seed(seed)
   rand_state.shuffle(b)

def saveToFile(model , fname):
    model_json = model.to_json()
    with open(fname, "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")

def build(width, height, depth, classes):
		# initialize the model
    model = Sequential()
    inputShape = (height, width, depth)

    # if we are using "channels first", update the input shape
    if K.image_data_format() == "channels_first":
        inputShape = (depth, height, width)

    model.add(Conv2D(32, (3, 3), padding="same", input_shape=inputShape))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.add(Dropout(0.25))
    # second set of CONV => RELU => POOL layers
    model.add(Conv2D(64, (4, 4), padding="same"))
    #model.add(Activation("relu"))
    model.add(LeakyReLU())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.add(Dropout(0.25))

    model.add(Conv2D(64, (4, 4), padding="same"))
    #model.add(Activation("relu"))
    model.add(LeakyReLU())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.add(Dropout(0.25))
    
    # first (and only) set of FC => RELU layers
    model.add(Flatten())
    model.add(Dense(1024))
    model.add(Activation("relu"))

    model.add(Dropout(0.50))

    # softmax classifier
    model.add(Dense(classes))
    model.add(Activation("softmax"))

    # return the constructed network architecture
    return model

batch_size = 128
num_classes = 15
epochs = 30

# input image dimensions
img_rows, img_cols = 100 , 80


path = "train/*/*.jpg"
tPath = "test/*.jpg"

pList = glob(path)

arrList = []
yArr = []
for imgPath in pList :
  img = cv2.imread(imgPath)
  img = cv2.resize(img , (img_rows , img_cols))
  _class = int(imgPath.split("/")[-2])
  hotArr = [0 for i in range(15)]
  hotArr[_class] = 1
  arrList.append(img)
  yArr.append(hotArr)

shuffle(arrList , yArr , 20)

arrListTrain , arrListTest = arrList[:int(0.8*(len(arrList)))] , arrList[int(0.8*(len(arrList))):]
yListTrain , yListTest =  yArr[:int(0.8*(len(yArr)))] , yArr[int(0.8*(len(yArr))):]

x_train = np.array(arrListTrain)
x_test = np.array(arrListTest)
y_train = np.array(yListTrain)
y_test = np.array(yListTest)

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 3, img_cols, img_rows)
    x_test = x_test.reshape(x_test.shape[0], 3,  img_cols, img_rows)
    input_shape = (3, img_cols, img_rows)
else:
    x_train = x_train.reshape(x_train.shape[0], img_cols, img_rows, 3)
    x_test = x_test.reshape(x_test.shape[0], img_cols, img_rows, 3)
    input_shape = (img_cols, img_rows, 3)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')
'''
model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))
'''

model = build(img_rows , img_cols , 3 , num_classes)

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=1)

print(str(datetime.datetime.now()))
print('Test loss:', score[0])
print('Test accuracy:', score[1])

#pickle.dump(model , open("save.p" , "wb"))
saveToFile(model , "try3.json")
